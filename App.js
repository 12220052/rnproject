// import { StatusBar } from 'expo-status-bar';
import { StyleSheet, View, LogBox } from 'react-native';
import ProductContainer from './Screens/Products/ProductContainer';
import Header from './Shared/Header'
import { GestureHandlerRootView } from 'react-native-gesture-handler';

LogBox.ignoreAllLogs(true)

export default function App() {
  return (
    <GestureHandlerRootView>
      <View style={styles.container}>
        <Header />
        <ProductContainer />
      </View>
    </GestureHandlerRootView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
